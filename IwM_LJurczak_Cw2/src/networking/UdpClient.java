package networking;

import java.io.File;
import java.io.IOException;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetAddress;
import java.net.SocketAddress;
import java.net.SocketException;
import java.net.UnknownHostException;
import java.nio.file.Files;
import java.util.Scanner;
import java.util.logging.Level;
import java.util.logging.Logger;

import main.Packet;
import main.TimeHistory;
import main.Tools;

public class UdpClient {
	public static void main(String[] args) {
		DatagramSocket aSocket = null;
		Scanner scanner = null;
		try {
			// args contain server hostname
			if (args.length < 1) {
				System.out.println("Usage: UDPClient <server host name>");
				System.exit(-1);
			}
			byte[] buffer = new byte[1024];
			InetAddress aHost = InetAddress.getByName(args[0]);
			int serverPort = 9876;
			aSocket = new DatagramSocket();
			scanner = new Scanner(System.in);
			String line = "";
			
			while (true) {
				System.out.println("Enter your message: ");
				if (scanner.hasNextLine())
					line = scanner.nextLine();
				Object[] tablica = new Object[2];		
				TimeHistory packet = new TimeHistory("Device name", "Description", 1000, 1, "m", 2000, tablica, 10);
				byte[] data = Tools.serialize(packet);
				DatagramPacket request = new DatagramPacket(line.getBytes(), line.length(), aHost, serverPort);
				aSocket.send(request);
				DatagramPacket reply = new DatagramPacket(buffer, buffer.length);
				aSocket.receive(reply);
				System.out.println("Reply: " + new String(reply.getData(), 0, reply.getLength()));
				Packet read = (Packet) Tools.deserialize(data);
				System.out.println(read);
				
				String name = (read.getDevice() + "_" + read.getDescription() + "_" + read.getDate() + ".txt");				
				Files.write(new File(name).toPath(), reply.getData());
			}
		} catch (SocketException ex) {
			Logger.getLogger(UdpClient.class.getName()).log(Level.SEVERE, null, ex);
		} catch (UnknownHostException ex) {
			Logger.getLogger(UdpClient.class.getName()).log(Level.SEVERE, null, ex);
		} catch (IOException ex) {
			Logger.getLogger(UdpClient.class.getName()).log(Level.SEVERE, null, ex);
		} catch (ClassNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} finally {
			aSocket.close();
			scanner.close();
		}
		
		
	}
}