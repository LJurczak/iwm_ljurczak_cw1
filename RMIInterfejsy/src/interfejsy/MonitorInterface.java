package interfejsy;

import java.rmi.*;

import main.Spectrum;
import main.TimeHistory;


public interface MonitorInterface extends Remote {
	void rejestruj(TimeHistory<?> thistMessage) throws RemoteException;
	void rejestruj(Spectrum<?> specMessage) throws RemoteException;
//	void komunikuj ( String message ) throws RemoteException ;
//	boolean zarejestruj ( String nick ) throws RemoteException ;
}