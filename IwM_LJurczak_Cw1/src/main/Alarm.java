package main;

public class Alarm extends Packet {
	
	int channelNr;
	int threshold;
	int direction;
	
	public Alarm(String device_, String description_, long date_, int channelNr_, int threshold_, int direction_) {
		super(device_, description_, date_);
		this.channelNr = channelNr_;
		this.threshold = threshold_;
		this.direction = direction_;
	}
	public String toString() {
		return this.device + "\n" + this.description + "\n" + this.date + "\n" + this.channelNr + "\n" + this.threshold + "\n" + this.direction + "\n";
	}
}
