package main;

import java.io.Serializable;

public abstract class Packet implements Serializable{
	
	protected String device;
	protected String description;
	protected long date;
	
	public Packet(String device_, String description_, long date_) {
	this.device = device_;
	this.description = description_;
	this.date = date_;
	}
	
	public String toString() {
		return this.device + this.description + this.date;
	}
	
	public String getDevice() {
		return this.device;
	}
	
	public String getDescription() {
		return this.description;
	}
	
	public long getDate() {
		return this.date;
	}

}