package main;

public abstract class Sequence<T> extends Packet {
	
	protected int channelNr;
	protected String unit;
	protected double resolution;
	protected T[] buffer;
	
	public Sequence(String device_, String description_, long date_, int channelNr_, String unit_, double resolution_, T[] buffer_) {
		super(device_, description_, date_);
		this.channelNr = channelNr_;
		this.unit = unit_;
		this.resolution = resolution_;
		this.buffer = buffer_;
	}
	
	public String toString() {
		return this.channelNr + this.unit + this.resolution + this.buffer;
	}

}
