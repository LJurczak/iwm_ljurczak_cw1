package main;

public class TimeHistory<T> extends Sequence<T> {
	
	double sensitivity;
	
	public TimeHistory(String device_, String description_, long date_, int channelNr_, String unit_, double resolution_, T[] buffer, double sensitivity_) {
		super(device_, description_, date_, channelNr_, unit_, resolution_, buffer);
		this.sensitivity = sensitivity_;
	}
	
	public String toString() {
		return this.device + "\n" + this.description + "\n" + this.date + "\n" + this.channelNr + 
				"\n" + this.unit + "\n" + this.resolution + "\n" + this.buffer + "\n" + this.sensitivity + "\n";
	}

}