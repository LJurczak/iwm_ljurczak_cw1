package main;

public class Spectrum<T> extends Sequence<T> {
	
	Boolean scaling;
	
	public Spectrum(String device_, String description_, long date_, int channelNr_, String unit_, double resolution_, T[] buffer_, Boolean scaling_) {
		super(device_, description_, date_, channelNr_, unit_, resolution_, buffer_);
		this.scaling = scaling_;
	}
	public String toString() {
		return this.device + "\n" + this.description + "\n" + this.date + "\n" + this.channelNr + 
				"\n" + this.unit + "\n" + this.resolution + "\n" + this.buffer + "\n" + this.scaling + "\n";
	}
}