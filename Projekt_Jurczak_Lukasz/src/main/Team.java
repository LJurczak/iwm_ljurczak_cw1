package main;

public class Team {
	public int points = 0;
	public int fouls = 0;
	
	public Team(int points_, int fouls_) {
		this.points = points_;
		this.fouls = fouls_;
	}
}
