package pack;

import java.io.Serializable;

public class StatsPacket implements Serializable{
	public int blueTeamPoints;
	public int blueTeamFouls;
	public int redTeamPoints;
	public int redTeamFouls;
	public int blue1Points;
	public int blue2Points;
	public int blue3Points;
	public int blue4Points;
	public int blue5Points;
	public int blue1Fouls;
	public int blue2Fouls;
	public int blue3Fouls;
	public int blue4Fouls;
	public int blue5Fouls;
	public int red1Points;
	public int red2Points;
	public int red3Points;
	public int red4Points;
	public int red5Points;
	public int red1Fouls;
	public int red2Fouls;
	public int red3Fouls;
	public int red4Fouls;
	public int red5Fouls;
	
	public StatsPacket(int blueTeamPoints_,
			int blueTeamFouls_,
			int redTeamPoints_,
			int redTeamFouls_,
			int blue1Points_,
			int blue2Points_,
			int blue3Points_,
			int blue4Points_,
			int blue5Points_,
			int blue1Fouls_,
			int blue2Fouls_,
			int blue3Fouls_,
			int blue4Fouls_,
			int blue5Fouls_,
			int red1Points_,
			int red2Points_,
			int red3Points_,
			int red4Points_,
			int red5Points_,
			int red1Fouls_,
			int red2Fouls_,
			int red3Fouls_,
			int red4Fouls_,
			int red5Fouls_){
		this.blueTeamPoints = blueTeamPoints_;
		this.blueTeamFouls = blueTeamFouls_;
		this.redTeamPoints = redTeamPoints_;
		this.redTeamFouls = redTeamFouls_;
		this.blue1Points = blue1Points_;
		this.blue2Points = blue2Points_;
		this.blue3Points = blue3Points_;
		this.blue4Points = blue4Points_;
		this.blue5Points = blue5Points_;
		this.blue1Fouls = blue1Fouls_;
		this.blue2Fouls = blue2Fouls_;
		this.blue3Fouls = blue3Fouls_;
		this.blue4Fouls = blue4Fouls_;
		this.blue5Fouls = blue5Fouls_;
		this.red1Points = red1Points_;
		this.red2Points = red2Points_;
		this.red3Points = red3Points_;
		this.red4Points = red4Points_;
		this.red5Points = red5Points_;
		this.red1Fouls = red1Fouls_;
		this.red2Fouls = red2Fouls_;
		this.red3Fouls = red3Fouls_;
		this.red4Fouls = red4Fouls_;
		this.red5Fouls = red5Fouls_;
	}
	
	public String toString() {
		String[][] strArray = new String[3][2];
		strArray[0][0] = "sss";
		return strArray.toString();
	}
	
	public int getBlueTeamPoints() {
		return blueTeamPoints;
	}
	
	public int getBlueTeamFouls() {
		return blueTeamFouls;
	}
	
	public int getRedTeamPoints() {
		return redTeamPoints;
	}
	
	public int getRedTeamFouls() {
		return redTeamFouls;
	}
	
	public int getBlue1Points() {
		return blue1Points;
	}
	
	public int getBlue2Points() {
		return blue2Points;
	}
	
	public int getBlue3Points() {
		return blue3Points;
	}
	
	public int getBlue4Points() {
		return blue4Points;
	}
	
	public int getBlue5Points() {
		return blue5Points;
	}
	
	public int getBlue1Fouls() {
		return blue1Fouls;
	}
	
	public int getBlue2Fouls() {
		return blue2Fouls;
	}
	
	public int getBlue3Fouls() {
		return blue3Fouls;
	}
	
	public int getBlue4Fouls() {
		return blue4Fouls;
	}
	
	public int getBlue5Fouls() {
		return blue5Fouls;
	}
	public int getRed1Points() {
		return red1Points;
	}
	
	public int getRed2Points() {
		return red2Points;
	}
	
	public int getRed3Points() {
		return red3Points;
	}
	
	public int getRed4Points() {
		return red4Points;
	}
	
	public int getRed5Points() {
		return red5Points;
	}
	
	public int getRed1Fouls() {
		return red1Fouls;
	}
	
	public int getRed2Fouls() {
		return red2Fouls;
	}
	
	public int getRed3Fouls() {
		return red3Fouls;
	}
	
	public int getRed4Fouls() {
		return red4Fouls;
	}
	
	public int getRed5Fouls() {
		return red5Fouls;
	}
	
	public void statsTable() {
    	final Object[][] table = new String[17][];
    	table[0] = new String[] { " ", "Niebiescy", "Czerwoni" };
    	table[1] = new String[] { "Punkty", Integer.toString(blueTeamPoints), Integer.toString(redTeamPoints) };
    	table[2] = new String[] { "Faule w kwarcie", Integer.toString(blueTeamFouls), Integer.toString(redTeamFouls) };
    	table[3] = new String[] { " ", " ", " " };
    	table[4] = new String[] { "Niebiescy", "Punkty", "Faule" };
    	table[5] = new String[] { "1", Integer.toString(blue1Points), Integer.toString(blue1Fouls) };
    	table[6] = new String[] { "2", Integer.toString(blue2Points), Integer.toString(blue2Fouls) };
    	table[7] = new String[] { "3", Integer.toString(blue3Points), Integer.toString(blue3Fouls) };
    	table[8] = new String[] { "4", Integer.toString(blue4Points), Integer.toString(blue4Fouls) };
    	table[9] = new String[] { "5", Integer.toString(blue5Points), Integer.toString(blue5Fouls) };
    	table[10] = new String[] { " ", " ", " " };
    	table[11] = new String[] { "Czerwoni", "Punkty", "Faule" };
    	table[12] = new String[] { "1", Integer.toString(red1Points), Integer.toString(red1Fouls) };
    	table[13] = new String[] { "2", Integer.toString(red2Points), Integer.toString(red2Fouls) };
    	table[14] = new String[] { "3", Integer.toString(red3Points), Integer.toString(red3Fouls) };
    	table[15] = new String[] { "4", Integer.toString(red4Points), Integer.toString(red4Fouls) };
    	table[16] = new String[] { "5", Integer.toString(red5Points), Integer.toString(red5Fouls) };

    	for (final Object[] row : table) {
    	    System.out.format("%-15s%-15s%-15s\n", row);
    	}
	}
}
