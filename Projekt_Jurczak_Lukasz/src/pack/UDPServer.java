package pack;

import java.io.IOException;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.SocketException;
import java.util.logging.Level;
import java.util.logging.Logger;

import main.Player;
import main.Team;

public class UDPServer {
	Player blue1 = new Player(0, 0);
	Player blue2 = new Player(0, 0);
	Player blue3 = new Player(0, 0);
	Player blue4 = new Player(0, 0);
	Player blue5 = new Player(0, 0);
	Player red1 = new Player(0, 0);
	Player red2 = new Player(0, 0);
	Player red3 = new Player(0, 0);
	Player red4 = new Player(0, 0);
	Player red5 = new Player(0, 0);
	Team blue = new Team(0, 0);
	Team red = new Team(0, 0);
    public static void main(String[] args) throws ClassNotFoundException{
    	DatagramSocket aSocket = null;
      try {
        // args contain message content and server hostname
        aSocket = new DatagramSocket(9876);
        byte[] buffer = new byte[1024];
        
        int bPoints = 0;
        int rPoints = 0;
        int bFouls = 0;
        int rFouls = 0;
        int b1Points = 0;
        int b2Points = 0;
        int b3Points = 0;
        int b4Points = 0;
        int b5Points = 0;
        int r1Points = 0;
        int r2Points = 0;
        int r3Points = 0;
        int r4Points = 0;
        int r5Points = 0;
        int b1Fouls = 0;
        int b2Fouls = 0;
        int b3Fouls = 0;
        int b4Fouls = 0;
        int b5Fouls = 0;
        int r1Fouls = 0;
        int r2Fouls = 0;
        int r3Fouls = 0;
        int r4Fouls = 0;
        int r5Fouls = 0;
        int quarter = 0;

        while(true) {
          DatagramPacket request = new DatagramPacket(buffer, buffer.length);

          aSocket.receive(request);
          String t = new String(request.getData(), 0, request.getLength());

          if(t.equals("S")) {
        	  
        	Stats<Integer> stat = new Stats<Integer>(bPoints, bFouls, rPoints, rFouls, b1Points, b2Points, b3Points, b4Points, b5Points, b1Fouls, b2Fouls, b3Fouls, b4Fouls, b5Fouls, r1Points, r2Points, r3Points, r4Points, r5Points, r1Fouls, r2Fouls, r3Fouls, r4Fouls, r5Fouls);
        	byte[] data = Tools.serialize(stat);
        	DatagramPacket datag = new DatagramPacket(data, data.length, 
                  request.getAddress(), request.getPort());
            aSocket.send(datag); // wysy�a aktualne warto�ci statystyk
            
          }else if(t.equals("K")){
        	  
        	  String replyEnd;
        	  if(quarter<4) {
        		  replyEnd = "Koniec " + Integer.toString(quarter) + " kwarty.\n" + Integer.toString(quarter+1) + " kwarta.";
        		  quarter++; 
        		  bFouls = 0;
        		  rFouls = 0;
        	  } else {
        		  replyEnd = "Koniec meczu.";
        	  }
        	  DatagramPacket datagramEnd = new DatagramPacket(replyEnd.getBytes(), replyEnd.length(), request.getAddress(), request.getPort());
        	  aSocket.send(datagramEnd); // informacja o ko�cu kwarty lub meczu
        	  
          }else if(t.equals("start")){
        	  DatagramPacket startReply = request;
        	  aSocket.send(startReply); // potwierdzenie
        	  
              DatagramPacket requestInitialize = new DatagramPacket(buffer, buffer.length);
              aSocket.receive(requestInitialize); //wyzerowanie statystyk
        	  StatsPacket read = (StatsPacket) Tools.deserialize(requestInitialize.getData());
        	  bPoints = read.getBlueTeamPoints();
        	  rPoints = read.getRedTeamPoints();
        	  bFouls = read.getBlueTeamFouls();
        	  rFouls = read.getRedTeamFouls();
        	  b1Points = read.getBlue1Points();
        	  b2Points = read.getBlue2Points();
        	  b3Points = read.getBlue3Points();
        	  b4Points = read.getBlue4Points();
        	  b5Points = read.getBlue5Points();
        	  r1Points = read.getRed1Points();
        	  r2Points = read.getRed2Points();
        	  r3Points = read.getRed3Points();
        	  r4Points = read.getRed4Points();
        	  r5Points = read.getRed5Points();
        	  b1Fouls = read.getBlue1Fouls();
        	  b2Fouls = read.getBlue2Fouls();
        	  b3Fouls = read.getBlue3Fouls();
        	  b4Fouls = read.getBlue4Fouls();
        	  b5Fouls = read.getBlue5Fouls();
        	  r1Fouls = read.getRed1Fouls();
        	  r2Fouls = read.getRed2Fouls();
        	  r3Fouls = read.getRed3Fouls();
        	  r4Fouls = read.getRed4Fouls();
        	  r5Fouls = read.getRed5Fouls();
        	  String replyStart = "Mecz rozpocz�ty.\n1 kwarta";
        	  quarter = 1;
        	  DatagramPacket datagramStart = new DatagramPacket(replyStart.getBytes(), replyStart.length(), request.getAddress(), request.getPort());
        	  aSocket.send(datagramStart); // informacja o pocz�tku meczu
        	  
          }else { // ka�de zdarzenie

        	  DatagramPacket requestEvent = request;

        	  StatsPacket read = (StatsPacket) Tools.deserialize(requestEvent.getData());
        	  bPoints += read.getBlueTeamPoints();
        	  rPoints += read.getRedTeamPoints();
        	  bFouls += read.getBlueTeamFouls();
        	  rFouls += read.getRedTeamFouls();
        	  b1Points += read.getBlue1Points();
        	  b2Points += read.getBlue2Points();
        	  b3Points += read.getBlue3Points();
        	  b4Points += read.getBlue4Points();
        	  b5Points += read.getBlue5Points();
        	  r1Points += read.getRed1Points();
        	  r2Points += read.getRed2Points();
        	  r3Points += read.getRed3Points();
        	  r4Points += read.getRed4Points();
        	  r5Points += read.getRed5Points();
        	  b1Fouls += read.getBlue1Fouls();
        	  b2Fouls += read.getBlue2Fouls();
        	  b3Fouls += read.getBlue3Fouls();
        	  b4Fouls += read.getBlue4Fouls();
        	  b5Fouls += read.getBlue5Fouls();
        	  r1Fouls += read.getRed1Fouls();
        	  r2Fouls += read.getRed2Fouls();
        	  r3Fouls += read.getRed3Fouls();
        	  r4Fouls += read.getRed4Fouls();
        	  r5Fouls += read.getRed5Fouls();
        	          	  
        	  String replyEvent = "";
        	  
        	  
        	  // sprawdzenie czy nie zosta� przekroczony limit fauli dru�yny
        	  if((bFouls >= 5 || rFouls >= 5)&&(read.getBlueTeamPoints() == 0)&&(read.getRedTeamPoints() == 0)) {
        		  String replyLimit = "limit";
        		  DatagramPacket datagramEvent = new DatagramPacket(replyLimit.getBytes(), replyLimit.length(), requestEvent.getAddress(), requestEvent.getPort());
            	  aSocket.send(datagramEvent); // informacja o limicie
            	  DatagramPacket replyLimitDatag = new DatagramPacket(buffer, buffer.length);
            	  aSocket.receive(replyLimitDatag); // ile trafionych rzut�w wolnych
            	  String tLimit = new String(replyLimitDatag.getData(), 0, replyLimitDatag.getLength());
            	  int intLimit = Integer.parseInt(tLimit);
            	  if(rFouls>=5) {
            		  bPoints += intLimit;
            	  }
            	  if(bFouls>=5) {
            		  rPoints += intLimit;
            	  }
        	  }
        	  
        	  
        	  //sprawdzenie czy nie zosta� przekroczony limit fauli ka�dego zawodnika
        	  if(b1Fouls >= 5) {
        		  replyEvent += "Zmiana zawodnika numer " + Integer.toString(b1Fouls) + " Limit 5 fauli.\n";
        		  b1Fouls = 0;
        		  b1Points = 0;
        	  }
        	  if(b2Fouls >= 5) {
        		  replyEvent += "Zmiana zawodnika numer " + Integer.toString(b2Fouls) + " Limit 5 fauli.\n";
        		  b2Fouls = 0;
        		  b2Points = 0;
        	  }
        	  if(b3Fouls >= 5) {
        		  replyEvent += "Zmiana zawodnika numer " + Integer.toString(b3Fouls) + " Limit 5 fauli.\n";
        		  b3Fouls = 0;
        		  b3Points = 0;
        	  }
        	  if(b4Fouls >= 5) {
        		  replyEvent += "Zmiana zawodnika numer " + Integer.toString(b4Fouls) + " Limit 5 fauli.\n";
        		  b4Fouls = 0;
        		  b4Points = 0;
        	  }
        	  if(b5Fouls >= 5) {
        		  replyEvent += "Zmiana zawodnika numer " + Integer.toString(b5Fouls) + " Limit 5 fauli.\n";
        		  b5Fouls = 0;
        		  b5Points = 0;
        	  }
        	  if(r1Fouls >= 5) {
        		  replyEvent += "Zmiana zawodnika numer " + Integer.toString(r1Fouls) + " Limit 5 fauli.\n";
        		  r1Fouls = 0;
        		  r1Points = 0;
        	  }
        	  if(r2Fouls >= 5) {
        		  replyEvent += "Zmiana zawodnika numer " + Integer.toString(r2Fouls) + " Limit 5 fauli.\n";
        		  r2Fouls = 0;
        		  r2Points = 0;
        	  }
        	  if(r3Fouls >= 5) {
        		  replyEvent += "Zmiana zawodnika numer " + Integer.toString(r3Fouls) + " Limit 5 fauli.\n";
        		  r3Fouls = 0;
        		  r3Points = 0;
        	  }
        	  if(r4Fouls >= 5) {
        		  replyEvent += "Zmiana zawodnika numer " + Integer.toString(r4Fouls) + " Limit 5 fauli.\n";
        		  r4Fouls = 0;
        		  r4Points = 0;
        	  }
        	  if(r5Fouls >= 5) {
        		  replyEvent += "Zmiana zawodnika numer " + Integer.toString(r5Fouls) + " Limit 5 fauli.\n";
        		  r5Fouls = 0;
        		  r5Points = 0;
        	  }
        	  
        	  
        	  
        	  replyEvent += "Zdarzenie zarejestrowane.\n";
        	  DatagramPacket datagramEvent = new DatagramPacket(replyEvent.getBytes(), replyEvent.length(), requestEvent.getAddress(), requestEvent.getPort());
        	  aSocket.send(datagramEvent); // potwierdzenie zdarzenia
          }
        }
      } catch (SocketException ex) {
        Logger.getLogger(UDPServer.class.getName()).log(Level.SEVERE, null, ex);
      } catch (IOException ex) {
        Logger.getLogger(UDPServer.class.getName()).log(Level.SEVERE, null, ex);
      } finally {
				aSocket.close();
			}      
    }
}
