package pack;

import java.io.IOException;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetAddress;
import java.net.SocketException;
import java.net.UnknownHostException;
import java.util.Scanner;
import java.util.logging.Level;
import java.util.logging.Logger;


public class UDPClient {
	public static void main(String[] args) throws ClassNotFoundException {
		DatagramSocket aSocket = null;
		Scanner scanner = null;
		Scanner scannerTeam = null;
		Scanner scannerEvent = null;
		Scanner scannerPlayer = null;
		Scanner scannerFree = null;
		try {

			byte[] buffer = new byte[1024];
			InetAddress aHost = InetAddress.getByName(args[0]);
			int serverPort = 9876;
			aSocket = new DatagramSocket();
			scanner = new Scanner(System.in);
			
	        int bPoints = 0;
	        int rPoints = 0;
	        int bFouls = 0;
	        int rFouls = 0;
	        int b1Points = 0;
	        int b2Points = 0;
	        int b3Points = 0;
	        int b4Points = 0;
	        int b5Points = 0;
	        int r1Points = 0;
	        int r2Points = 0;
	        int r3Points = 0;
	        int r4Points = 0;
	        int r5Points = 0;
	        int b1Fouls = 0;
	        int b2Fouls = 0;
	        int b3Fouls = 0;
	        int b4Fouls = 0;
	        int b5Fouls = 0;
	        int r1Fouls = 0;
	        int r2Fouls = 0;
	        int r3Fouls = 0;
	        int r4Fouls = 0;
	        int r5Fouls = 0;

			String line = "";

			while (true) {
				System.out.println("start - rozpocznij mecz\n"
						+ "K - zako�cz kwart�\n"
						+ "N - zdarzenie w dru�ynie niebieskich\n"
						+ "C - zdarzenie w dru�ynie czerwonych\n"
						+ "S - wy�wietl statystyki\n");
				if (scanner.hasNextLine())
					line = scanner.nextLine();
				
				if(line.equals("start")) {

					DatagramPacket requestStart = new DatagramPacket(line.getBytes(), line.length(), aHost, serverPort);
					aSocket.send(requestStart); // rozpocz�cie meczu
					DatagramPacket replyStart = new DatagramPacket(buffer, buffer.length);
					aSocket.receive(replyStart); // potwierdzenie
					Stats<Integer> startPacket = new Stats<Integer>(0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0);
					byte[] data = Tools.serialize(startPacket);
					DatagramPacket request = new DatagramPacket(data, data.length, aHost, serverPort);
					aSocket.send(request); // wyzerowanie statystyk
					DatagramPacket reply = new DatagramPacket(buffer, buffer.length);
					aSocket.receive(reply); // potwierdzenie
					System.out.println(new String(reply.getData(), 0, reply.getLength()));
				}
				
				if(line.equals("K")) {
					DatagramPacket request = new DatagramPacket(line.getBytes(), line.length(), aHost, serverPort);
					aSocket.send(request); // koniec kwarty
					DatagramPacket reply = new DatagramPacket(buffer, buffer.length);
					aSocket.receive(reply); // informacja o numerze kwarty lub ko�cu meczu
					System.out.println(new String(reply.getData(), 0, reply.getLength()));
				}
				
				if (line.equals("N") || line.equals("C")) {
					
				    if (line.equals("N")) {
				        System.out.println("2P - zdobyte 2 punkty\n" +
				            "3P - zdobyte 3 punkty\n" +
				            "F - pope�niony faul\n");
				        scannerTeam = new Scanner(System.in);
				        if (scannerTeam.hasNextLine())
				            line = scannerTeam.nextLine();

				        if (line.equals("2P")) {
				            System.out.print("Wybierz numer zawodnika 1-5\n");
				            scannerPlayer = new Scanner(System.in);
				            if (scannerPlayer.hasNextLine())
				                line = scannerPlayer.nextLine();
				            bPoints += 2;
				            switch (line) {
				            case "1":
				            	b1Points += 2;
				            	break;
				            case "2":
				            	b2Points += 2;
				            	break;
				            case "3":
				            	b3Points += 2;
				            	break;
				            case "4":
				            	b4Points += 2;
				            	break;
				            case "5":
				            	b5Points += 2;
				            	break;
				            }
				            
				        } else if (line.equals("3P")) {
				            System.out.print("Wybierz numer zawodnika 1-5\n");
				            scannerPlayer = new Scanner(System.in);
				            if (scannerPlayer.hasNextLine())
				                line = scannerPlayer.nextLine();
				            bPoints += 3;
				            switch (line) {
				            case "1":
				            	b1Points += 3;
				            	break;
				            case "2":
				            	b2Points += 3;
				            	break;
				            case "3":
				            	b3Points += 3;
				            	break;
				            case "4":
				            	b4Points += 3;
				            	break;
				            case "5":
				            	b5Points += 3;
				            	break;
				            }				            
				            
				        } else if (line.equals("F")) {
				            System.out.print("Wybierz numer zawodnika 1-5\n");
				            scannerPlayer = new Scanner(System.in);
				            if (scannerPlayer.hasNextLine())
				                line = scannerPlayer.nextLine();
				            bFouls += 1;
				            switch (line) {
				            case "1":
				            	b1Fouls += 1;
				            	break;
				            case "2":
				            	b2Fouls += 1;
				            	break;
				            case "3":
				            	b3Fouls += 1;
				            	break;
				            case "4":
				            	b4Fouls += 1;
				            	break;
				            case "5":
				            	b5Fouls += 1;
				            	break;
				            }

				        } else {
				            System.out.print("Z�a komenda.");
				            continue;
				        }
				        
				    }else if(line.equals("C")) {
				        System.out.println("2P - zdobyte 2 punkty\n" +
					            "3P - zdobyte 3 punkty\n" +
					            "F - pope�niony faul\n");
					        scannerTeam = new Scanner(System.in);
					        if (scannerTeam.hasNextLine())
					            line = scannerTeam.nextLine();

					        if (line.equals("2P")) {
					            System.out.print("Wybierz numer zawodnika 1-5\n");
					            scannerPlayer = new Scanner(System.in);
					            if (scannerPlayer.hasNextLine())
					                line = scannerPlayer.nextLine();
					            rPoints += 2;
					            switch (line) {
					            case "1":
					            	r1Points += 2;
					            	break;
					            case "2":
					            	r2Points += 2;
					            	break;
					            case "3":
					            	r3Points += 2;
					            	break;
					            case "4":
					            	r4Points += 2;
					            	break;
					            case "5":
					            	r5Points += 2;
					            	break;
					            }
					            
					        } else if (line.equals("3P")) {
					            System.out.print("Wybierz numer zawodnika 1-5\n");
					            scannerPlayer = new Scanner(System.in);
					            if (scannerPlayer.hasNextLine())
					                line = scannerPlayer.nextLine();
					            rPoints += 3;
//					            System.out.println(line);
					            switch (line) {
					            case "1":
					            	r1Points += 3;
					            	break;
					            case "2":
					            	r2Points += 3;
					            	break;
					            case "3":
					            	r3Points += 3;
					            	break;
					            case "4":
					            	r4Points += 3;
					            	break;
					            case "5":
					            	r5Points += 3;
					            	break;					            	
					            }				            
					        } else if (line.equals("F")) {
					            System.out.print("Wybierz numer zawodnika 1-5\n");
					            scannerPlayer = new Scanner(System.in);
					            if (scannerPlayer.hasNextLine())
					                line = scannerPlayer.nextLine();
					            rFouls += 1;
					            switch (line) {
					            case "1":
					            	r1Fouls += 1;
					            	break;
					            case "2":
					            	r2Fouls += 1;
					            	break;
					            case "3":
					            	r3Fouls += 1;
					            	break;
					            case "4":
					            	r4Fouls += 1;
					            	break;
					            case "5":
					            	r5Fouls += 1;
					            	break;
					            }

					        } else {
					            System.out.print("Z�a komenda.");
					            continue;
					        }
				    }
				    //wys�anie dla C lub N
				    Stats<Integer> eventPacket = new Stats<Integer>(bPoints,bFouls,rPoints,rFouls,b1Points,b2Points,b3Points,b4Points,b5Points,b1Fouls,b2Fouls,b3Fouls,b4Fouls,b5Fouls,r1Points,r2Points,r3Points,r4Points,r5Points,r1Fouls,r2Fouls,r3Fouls,r4Fouls,r5Fouls);
					byte[] data = Tools.serialize(eventPacket);
					DatagramPacket requestEvent = new DatagramPacket(data, data.length, aHost, serverPort);
					aSocket.send(requestEvent); // ilo�� punkt�w lub faul i kogo to dotyczy
					DatagramPacket replyEvent = new DatagramPacket(buffer, buffer.length);
					aSocket.receive(replyEvent);    // otrzymana informacja czy osi�gni�to limit lub o zarejestrowanym zdarzeniu
					String lim = new String(replyEvent.getData(), 0, replyEvent.getLength());
					if(lim.equals("limit")) { // gdy osi�gni�to limit dyktowan� d� 2 rzuty wolne
						System.out.print("Limit fauli dru�yny. 2 rzuty wolne. Wpisz ilo�� trafionych.\n");
						scannerFree = new Scanner(System.in);
			            if (scannerFree.hasNextLine())
			                line = scannerFree.nextLine();
			            DatagramPacket limit = new DatagramPacket(line.getBytes(), line.length(), requestEvent.getAddress(), requestEvent.getPort());
		            	  aSocket.send(limit); // informacja o ilo�ci trafionych 0-2
		            	  aSocket.receive(replyEvent); // zarejestrowane zdarzenie
					}
					System.out.println(new String(replyEvent.getData(), 0, replyEvent.getLength()));
					bPoints = 0;
					rPoints = 0; // reset warto�ci dla zdarzenia w nast�pnej iteracji
					bFouls = 0;
					rFouls = 0;
					b1Points = 0;
					b2Points = 0;
					b3Points = 0;
					b4Points = 0;
					b5Points = 0;
					r1Points = 0;
					r2Points = 0;
					r3Points = 0;
					r4Points = 0;
					r5Points = 0;
					b1Fouls = 0;
					b2Fouls = 0;
					b3Fouls = 0;
					b4Fouls = 0;
					b5Fouls = 0;
					r1Fouls = 0;
					r2Fouls = 0;
					r3Fouls = 0;
					r4Fouls = 0;
					r5Fouls = 0;
				}
				
				if(line.equals("S")) {
					DatagramPacket request = new DatagramPacket(line.getBytes(), line.length(), aHost, serverPort);
					aSocket.send(request);
					DatagramPacket reply = new DatagramPacket(buffer, buffer.length);
					aSocket.receive(reply); // aktualne statystyki
					StatsPacket read;
					read = (StatsPacket) Tools.deserialize(reply.getData());
					read.statsTable();

				}
				
			}
		} catch (SocketException ex) {
			Logger.getLogger(UDPClient.class.getName()).log(Level.SEVERE, null, ex);
		} catch (UnknownHostException ex) {
			Logger.getLogger(UDPClient.class.getName()).log(Level.SEVERE, null, ex);
		} catch (IOException ex) {
			Logger.getLogger(UDPClient.class.getName()).log(Level.SEVERE, null, ex);
		} finally {
			aSocket.close();
			scanner.close();
		}
	}
}