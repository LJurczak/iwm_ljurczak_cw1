package klient;

import java.rmi.NotBoundException;
import java.rmi.RemoteException;
import java.rmi.registry.LocateRegistry;
import java.rmi.registry.Registry ;
import interfejsy.MonitorInterface;
import main.Spectrum;
import main.TimeHistory;

public class MonitorClient{
	public static void main (String[] args){
		Integer tablica[]={1,1,1};
		TimeHistory<Integer> packetThist = new TimeHistory<Integer>("Device name", "Description", 1000, 1, "m", 2000, tablica, 10);
		Spectrum<Integer> packetSpec = new Spectrum<Integer>("Device name", "Description", 1000, 1, "m", 2000, tablica, true);
		
		MonitorInterface remoteObject; // referencja do zdalnego obiektu
		Registry reg; // rejestr nazw obiektow
		try{
			reg = LocateRegistry.getRegistry(args[0]);
			remoteObject = (MonitorInterface)reg.lookup("MonitorServer");
			remoteObject.rejestruj(packetThist);
			remoteObject.rejestruj(packetSpec);

//			remoteObject.komunikuj("Hi");
//			remoteObject.zarejestruj("Luc");
//			System.out.println(reg);
		}
		catch(RemoteException e){
			e.printStackTrace();
		}
		catch(NotBoundException e){
			e.printStackTrace();
		}
	}
}