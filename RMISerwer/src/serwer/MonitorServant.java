package serwer;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.nio.file.Files;
import java.rmi.RemoteException;
import java.rmi.server.UnicastRemoteObject;
import java.util.LinkedList;
import java.util.List;

import interfejsy.MonitorInterface;
import main.Spectrum;
import main.TimeHistory;

public class MonitorServant 
	extends UnicastRemoteObject
	implements MonitorInterface{

	List<TimeHistory<?>> thistList = new LinkedList<>();
	List<Spectrum<?>> specList = new LinkedList<>();
	
	public MonitorServant () throws RemoteException {
		}

//	public void komunikuj (String message) throws RemoteException {
//		System.out.println ("Server.komunikuj() " + message );
//	}
//	public boolean zarejestruj (String nick) throws RemoteException {
//		System.out.println ("Server.zarejestruj() " + nick );
//		return true ;
//		}
	@Override
	public void rejestruj(TimeHistory<?> x) throws RemoteException {
				
		thistList.add(x);		
		String name = (x.getDevice() + "_" + x.getDescription() + "_" + x.getDate() + ".thi");	

		try {
			Files.write(new File(name).toPath(), Tools.serialize(x));
//			System.out.println(x);
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
	}
	@Override
	public void rejestruj(Spectrum<?> x) throws RemoteException {
		// TODO Auto-generated method stub
		specList.add(x);		
		String name = (x.getDevice() + "_" + x.getDescription() + "_" + x.getDate() + ".spc");	

		try {
			Files.write(new File(name).toPath(), Tools.serialize(x));
//			System.out.println(x);
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
	}

}
